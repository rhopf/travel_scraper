# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 16:42:15 2017

@author: rhopf
"""

# custom modules and settings
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

# import selenium driver and helpers
from selenium import webdriver
from bs4 import BeautifulSoup
import threading
import os

# global vars
url = "http://www.statravel.ch/"
t_wait_long  = 30

# main scraping class
class STAScrape(object):
    
    def __init__(self, dep_ap, des_ap, dep_date, ret_date):
        # init
        self.dep_ap = dep_ap
        self.des_ap = des_ap
        self.dep_date = dep_date
        self.ret_date = ret_date
        
        # set browser driver
        self.driver = webdriver.Chrome("C:/Temp/chromedriver.exe")
        
    def open_page(self):
        # open url
        self.driver.get(url)
        self.driver.implicitly_wait(t_wait_long)
        
    def fill_and_submit(self):

        # select forms
        depart_input = self.driver.find_element_by_css_selector(".flight_depart_location.ui-autocomplete-input")
        destin_input = self.driver.find_element_by_css_selector(".flight_arrive_location.ui-autocomplete-input")
        
        # send text
        depart_input.send_keys(self.dep_ap)
        self.driver.find_element_by_id("ui-id-4").click()
        destin_input.send_keys(self.des_ap)
        self.driver.find_element_by_id("ui-id-5").click()
        
        # datum click: needs some prework to determine where div[i]tr[j]td[k] map onto by date!
        self.driver.find_element_by_xpath("//*[@id=\"ui-datepicker-div\"]/div[2]/table/tbody/tr[3]/td[4]/a").click()
        self.driver.find_element_by_xpath("//*[@id=\"ui-datepicker-div\"]/div[2]/div/a/span").click()
        self.driver.find_element_by_xpath("//*[@id=\"ui-datepicker-div\"]/div[2]/table/tbody/tr[3]/td[7]/a").click()
        
        destin_date = self.driver.find_element_by_class_name("adults")
        destin_date.send_keys("1")
        
        # search!
        search_button = self.driver.find_element_by_xpath("//*[@id=\"qbtb_2_0_id_0\"]/div/div[1]/div[2]/div[2]/form/fieldset[6]/p/button")
        search_button.click()
        
    def scrape_results(self, index_cut):
        # load data
        self.driver.implicitly_wait(t_wait_long)
        self.data = self.driver.find_elements_by_css_selector(".filterListLabel")
        
        self.airlines = list()
        self.prices   = list()
        
        for data_i in self.data[index_cut:]:
            soup = BeautifulSoup(data_i.get_attribute("innerHTML"), "lxml")
            self.airlines.append(soup.findAll(attrs={'class' : 'filterText'})[0].contents[0].strip())
            self.prices.append(int(soup.findAll(attrs={'class' : 'price'})[0].contents[0].strip().split("CHF ")[-1].split(".")[0]))
        
    def close_browser(self):
        self.driver.close()
        
    def process_scraped_data(self):
        # sort shit
        sorter   = np.argsort(self.prices)
        self.prices   = [self.prices[i] for i in sorter]
        self.airlines = [self.airlines[i] for i in sorter]
        
    def plot_data(self):
        # plot 
        fsize = 18
        plt.figure(figsize=(18,10))
        plt.bar(range(len(self.prices)), self.prices)
        # plt.legend(loc='best', fontsize=fsize, fancybox=True, framealpha=0.0)
        plt.title('Flight ZH-BJS', fontsize=fsize, y=1.01)
        # plt.xlabel(r'number [-]', fontsize=fsize, labelpad=10)
        plt.ylabel('price [CHF]', fontsize=fsize, labelpad=10)
        plt.xticks(np.arange(len(self.prices)), self.airlines, rotation=90)
        # plt.yticks([], [])
        plt.tick_params(labelsize=fsize, labelcolor='k', pad=10)
        plt.tight_layout()
        plt.show()
        
    def save_data(self):
        if not os.path.isdir("data"):
            os.mkdir("data")
        f = open(os.path.join("data", "_".join(self.dep_ap.split(" ")) + "_to_" + "_".join(self.des_ap.split(" ")) + ".dat"), "w+")
        for a1, a2 in zip(self.airlines, self.prices):
            f.write(a1 + "," + str(a2) + "\n")
        f.close()
    
    # multirun tool
    def run_all(self, index_cut):
        self.open_page()
        self.fill_and_submit()
        self.scrape_results(index_cut)
        self.close_browser()
        self.process_scraped_data()
        self.save_data()
        self.plot_data()
        
    # multirun tool 2
    def run_all_noplot(self, index_cut):
        self.open_page()
        self.fill_and_submit()
        self.scrape_results(index_cut)
        self.close_browser()
        self.process_scraped_data()
        self.save_data()

        
# run! ========================================================================
if __name__ == "__main__":
    
    # set destinations
    destinations = ["peking", "tokio nar", "bang"]
    
    # init lists
    scrapers = []
    threads  = []
    for dest_i in destinations:
        scrapers.append(STAScrape("zur", dest_i, "fg", "xx"))
        t = threading.Thread(target=scrapers[-1].run_all_noplot, args=(3,))
        threads.append(t)
        t.start()


